#!/usr/bin/env python

import argparse
import sys

from imp_parser import imp_lex, imp_parse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=argparse.FileType("r"), default=sys.stdin)
    args = parser.parse_args()

    text = args.filename.read()
    tokens = imp_lex(text)
    result = imp_parse(tokens)
    if not result:
        print("Unknown parse error!", file=sys.stderr)
        sys.exit(1)

    ast = result.value
    env = {}
    ast.eval(env)

    print("Final variable values:")
    for name, value in env.items():
        print(f" {name}: {value}")
