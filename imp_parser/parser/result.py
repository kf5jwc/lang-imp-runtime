from dataclasses import dataclass
from typing import Sequence, Optional


@dataclass
class Result:
    value: Optional[Sequence[str]]
    pos: int
