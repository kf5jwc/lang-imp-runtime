from dataclasses import dataclass
from typing import Callable, Union, Optional, Any, Tuple
from .result import Result
from ..imp_lexer import Tags


class Parser(object):
    def __call__(self: "Parser", tokens: BaseException, pos: int) -> Any:
        raise NotImplementedError

    # oooh, fun. Operator overloading can get confusing.
    def __add__(self: "Parser", other: "Parser") -> "Concat":
        return Concat(self, other)

    def __mul__(self: "Parser", other: "Process") -> "Exp":
        return Exp(self, other)

    def __or__(self: "Parser", other: Union["Tag", "Parser"]) -> "Alternate":
        return Alternate(self, other)

    def __xor__(self: "Parser", other: Callable) -> "Process":
        return Process(self, other)


@dataclass
class Reserved(Parser):
    value: str
    tag: Tags

    def __call__(self, tokens, pos) -> Optional[Any]:
        if pos < len(tokens):
            (value, tag) = tokens[pos]
            if value == self.value and tag == self.tag:
                return Result(value, pos + 1)

        return None


@dataclass
class Tag(Parser):
    tag: Tags

    def __call__(self, tokens, pos) -> Optional[Any]:
        if pos < len(tokens):
            (value, tag) = tokens[pos]
            if tag is self.tag:
                return Result(value, pos + 1)

        return None


@dataclass
class Concat(Parser):
    left: Parser
    right: Parser

    def __call__(self, tokens, pos) -> Optional[Any]:
        left_result = self.left(tokens, pos)
        if left_result is None:
            return None

        right_result = self.right(tokens, left_result.pos)
        if right_result is None:
            return None

        combined_result = (left_result.value, right_result.value)
        return Result(combined_result, right_result.pos)


@dataclass
class Exp(Parser):
    parser: Parser
    separator: "Process"

    def __call__(self, tokens, pos) -> Any:
        def process_next(parsed):
            (sepfunc, right) = parsed
            return sepfunc(result.value, right)

        result = self.parser(tokens, pos)
        next_parser: Process = self.separator + self.parser ^ process_next
        next_result = result

        while next_result:
            next_result = next_parser(tokens, result.pos)
            if next_result:
                result = next_result

        return result


@dataclass
class Alternate(Parser):
    left: Parser
    right: Parser

    def __call__(self, tokens, pos) -> Any:
        left_result = self.left(tokens, pos)
        if left_result is not None:
            return left_result
        return self.right(tokens, pos)


@dataclass
class Opt(Parser):
    parser: Parser

    def __call__(self, tokens, pos) -> Result:
        result = self.parser(tokens, pos)
        if result is not None:
            return result
        return Result(None, pos)


@dataclass
class Rep(Parser):
    parser: Parser

    def __call__(self, tokens, pos) -> Result:
        results = []
        result = self.parser(tokens, pos)

        while result is not None:
            results.append(result.value)
            pos = result.pos
            result = self.parser(tokens, pos)

        return Result(results, pos)


@dataclass
class Process(Parser):
    parser: Parser
    function: Callable

    def __call__(self, tokens, pos) -> Optional[Result]:
        result: Optional[Result] = self.parser(tokens, pos)
        if result is not None:
            result.value = self.function(result.value)
        return result


@dataclass
class Lazy(Parser):
    parser_func: Callable[..., Parser]
    parser: Optional[Parser] = None

    def __call__(self, tokens, pos) -> Any:
        if not self.parser:
            self.parser = self.parser_func()
        return self.parser(tokens, pos)


@dataclass
class Phrase(Parser):
    parser: Parser

    def __call__(self, tokens, pos) -> Optional[Any]:
        result = self.parser(tokens, pos)

        if result is not None:
            if result.pos == len(tokens):
                return result

        return None
