from typing import Any, Callable, Union, Tuple
from ..ast import Bexp, AndBexp, NotBexp, OrBexp, RelopBexp
from .arithmetic import aexp, any_operator_in_list, keyword, precedence, process_group
from .combinators import Alternate, Process, Lazy


RELOPS = ["<", "<=", ">", ">=", "=", "!="]
BEXP_PRECEDENCE_LEVELS = [["and"], ["or"]]


def process_relop(parsed: Tuple[Tuple[Any, str], Any]) -> RelopBexp:
    ((left, op), right) = parsed
    return RelopBexp(op, left, right)


def bexp_relop() -> Process:
    return aexp() + any_operator_in_list(RELOPS) + aexp() ^ process_relop


def bexp_not() -> Process:
    return keyword("not") + Lazy(bexp_term) ^ (lambda parsed: NotBexp(parsed[1]))


def bexp_group() -> Process:
    return keyword("(") + Lazy(bexp) + keyword(")") ^ process_group


def bexp_term() -> Alternate:
    return bexp_not() | bexp_relop() | bexp_group()


def process_logic(op: str) -> Callable[[Bexp, Bexp], Union[AndBexp, OrBexp]]:
    if op == "and":
        return lambda l, r: AndBexp(l, r)
    elif op == "or":
        return lambda l, r: OrBexp(l, r)
    else:
        raise RuntimeError(f"Unknown logic operator: {op}")


def bexp():
    return precedence(bexp_term(), BEXP_PRECEDENCE_LEVELS, process_logic)
