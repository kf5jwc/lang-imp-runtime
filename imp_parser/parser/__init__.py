from .result import Result # It's weird, but this needs to be imported first-ish.
from .combinators import Phrase, Exp # This one needs to come second-ish?
from .statements import stmt_list
from .arithmetic import aexp


def parser() -> Phrase:
    return Phrase(stmt_list())
