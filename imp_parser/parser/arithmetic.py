from functools import reduce
from typing import Callable, List, Union
from ..ast import Aexp, IntAexp, BinopAexp, VarAexp
from ..imp_lexer import Tags
from .combinators import (
    Reserved,
    Tag,
    Concat,
    Exp,
    Alternate,
    Opt,
    Rep,
    Process,
    Lazy,
    Phrase,
)


# "we use the Process combinator (actually the ^ operator, which calls
#  Process) to convert the token into an actual integer value."
NUM = Tag(Tags.Int) ^ (lambda i: int(i))
ID = Tag(Tags.ID)
AEXP_PRECEDENCE_LEVELS = [["*", "/"], ["+", "-"]]


def keyword(kw: str) -> Reserved:
    return Reserved(kw, Tags.Reserved)


def aexp_value() -> Alternate:
    # Note! The pipe is shorthand for a combinator here.
    a = NUM ^ (lambda i: IntAexp(i))
    b = ID ^ (lambda v: VarAexp(v))
    return a | b


def process_group(parsed) -> int:
    ((_, p), _) = parsed
    return p


def aexp_group() -> Process:
    return keyword("(") + Lazy(aexp) + keyword(")") ^ process_group


def aexp_term() -> Alternate:
    return aexp_value() | aexp_group()


def process_binop(op) -> Callable[[Aexp, Aexp], BinopAexp]:
    return lambda l, r: BinopAexp(op, l, r)


def any_operator_in_list(ops) -> Reserved:
    op_parsers: List[Reserved] = [keyword(op) for op in ops]
    reducer: Callable[..., Reserved] = lambda l, r: l | r;
    parser = reduce(reducer, op_parsers)
    return parser


def precedence(value_parser, precedence_levels, combine):
    def op_parser(precedence_level):
        return any_operator_in_list(precedence_level) ^ combine

    parser = value_parser * op_parser(precedence_levels[0])

    for precedence_level in precedence_levels[1:]:
        parser = parser * op_parser(precedence_level)

    return parser


def aexp():
    return precedence(aexp_term(), AEXP_PRECEDENCE_LEVELS, process_binop)
