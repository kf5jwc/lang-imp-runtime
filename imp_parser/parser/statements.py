from ..ast import AssignStatement, CompoundStatement, IfStatement, WhileStatement, RelopBexp
from .arithmetic import aexp, ID, keyword
from .boolean import bexp
from .combinators import Alternate, Exp, Lazy, Opt, Process

from typing import Tuple, Any


def assign_stmt() -> Process:
    def process(parsed):
        ((name, _), exp) = parsed
        return AssignStatement(name, exp)

    return ID + keyword(":=") + aexp() ^ process


def stmt_list() -> Exp:
    separator = keyword(";") ^ (lambda x: lambda l, r: CompoundStatement(l, r))
    return Exp(stmt(), separator)


def if_stmt() -> Process:
    # Okay, so at this point I *hate* how this combinator return structure is organized.
    def process(parsed: Tuple[Tuple[Tuple[Tuple[Tuple[str, RelopBexp], str], AssignStatement], Tuple[str, AssignStatement]], str]):
        (((((_, condition), _), true_stmt), false_parsed), _) = parsed

        if false_parsed:
            (_, false_stmt) = false_parsed
        else:
            false_stmt = None

        return IfStatement(condition, true_stmt, false_stmt)

    return keyword("if") + bexp() \
         + keyword("then") + Lazy(stmt_list) \
         + Opt(keyword("else") + Lazy(stmt_list)) \
         + keyword("end") ^ process

def while_stmt() -> Process:
    def process(parsed):
        ((((_, condition), _), body), _) = parsed
        return WhileStatement(condition, body)

    return keyword("while") + bexp() \
         + keyword("do") + Lazy(stmt_list) \
         + keyword("end") ^ process


def stmt() -> Alternate:
    return assign_stmt() \
         | if_stmt() \
         | while_stmt()
