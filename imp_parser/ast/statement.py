from dataclasses import dataclass
from .types import Aexp, Statement
from .boolean import RelopBexp

@dataclass
class AssignStatement(Statement):
    name: str
    aexp: Aexp

    def eval(self, env: dict) -> None:
        value = self.aexp.eval(env)
        env[self.name] = value


@dataclass
class CompoundStatement(Statement):
    first: Statement
    second: Statement

    def eval(self, env: dict) -> None:
        self.first.eval(env)
        self.second.eval(env)


@dataclass
class IfStatement(Statement):
    condition: RelopBexp
    true_stmt: Statement
    false_stmt: Statement

    def eval(self, env: dict) -> None:
        condition_value: bool = self.condition.eval(env)
        if condition_value:
            self.true_stmt.eval(env)
        else:
            if self.false_stmt:
                self.false_stmt.eval(env)


@dataclass
class WhileStatement(Statement):
    condition: RelopBexp
    body: CompoundStatement

    def eval(self, env: dict) -> None:
        condition_value = self.condition.eval(env)
        while condition_value:
            self.body.eval(env)
            condition_value = self.condition.eval(env)
