from typing import Union
from ..parser import Result


class Equality(object):
    def __eq__(self, other) -> bool:
        return isinstance(other, self.__class__) and \
               self.__dict__ == other.__dict__

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)


# "A"rithmetic "exp"ression
class Aexp(Equality):

    def eval(self, env: dict) -> int:
        raise NotImplementedError


# "B"oolean "exp"ression
class Bexp(Equality):

    def eval(self, env: dict) -> bool:
        raise NotImplementedError


class Statement(Equality):

    def eval(self, env: dict) -> None:
        raise NotImplementedError
