from dataclasses import dataclass
from .types import Aexp, Bexp
from ..parser import Exp
from typing import Any, Union

@dataclass
class BinopAexp(Aexp):
    op: str
    left: Aexp
    right: Aexp

    def eval(self, env: dict) -> int:
        left_value: int = self.left.eval(env)
        right_value: int = self.right.eval(env)

        if   self.op == '+':
            return left_value + right_value
        elif self.op == '-':
            return left_value - right_value
        elif self.op == '*':
            return left_value * right_value
        elif self.op == '/':
            return int(left_value / right_value)

        raise RuntimeError(f"Unknown operator: {self.op}")


@dataclass
class RelopBexp(Bexp):
    op: str
    left: Bexp
    right: Bexp

    def eval(self, env: dict) -> bool:
        left_value: int = self.left.eval(env)
        right_value: int = self.right.eval(env)

        if   self.op == '<':
            return left_value <  right_value
        elif self.op == '<=':
            return left_value <= right_value
        elif self.op == '>':
            return left_value >  right_value
        elif self.op == '>=':
            return left_value >= right_value
        elif self.op == '=':
            return left_value == right_value
        elif self.op == '!=':
            return left_value != right_value

        raise RuntimeError(f"Unknown operator: {self.op}")


@dataclass
class AndBexp(Bexp):
    left: Bexp
    right: Bexp

    def eval(self, env: dict) -> bool:
        left_value = self.left.eval(env)
        right_value = self.right.eval(env)
        return (left_value and right_value)


@dataclass
class OrBexp(Bexp):
    left: Bexp
    right: Bexp

    def eval(self, env: dict) -> bool:
        left_value = self.left.eval(env)
        right_value = self.right.eval(env)
        return (left_value or right_value)


@dataclass
class NotBexp(Bexp):
    exp: Bexp

    def eval(self, env: dict) -> bool:
        value = self.exp.eval(env)
        return (not value)
