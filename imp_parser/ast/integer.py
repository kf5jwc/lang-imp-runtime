from dataclasses import dataclass
from .types import Aexp


@dataclass
class IntAexp(Aexp):
    i: int

    def eval(self, _: dict) -> int:
        return self.i


@dataclass
class VarAexp(Aexp):
    name: str

    def eval(self, env: dict) -> int:
        if self.name in env:
            return env[self.name]
        return 0
