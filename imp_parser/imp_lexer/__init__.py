from enum import Enum
from .. import lexer


class Tags(Enum):
    Reserved = 0
    Int = 1
    ID = 2


token_exprs = [
    (r"[ \n\t]", None),
    (r"#[^\n]*", None),
    (r"\:=", Tags.Reserved),
    (r"\(", Tags.Reserved),
    (r"\)", Tags.Reserved),
    (r";", Tags.Reserved),
    (r"\+", Tags.Reserved),
    (r"-", Tags.Reserved),
    (r"\*", Tags.Reserved),
    (r"/", Tags.Reserved),
    (r"<=", Tags.Reserved),
    (r"<", Tags.Reserved),
    (r">=", Tags.Reserved),
    (r">", Tags.Reserved),
    (r"=", Tags.Reserved),
    (r"!=", Tags.Reserved),
    (r"and", Tags.Reserved),
    (r"or", Tags.Reserved),
    (r"not", Tags.Reserved),
    (r"if", Tags.Reserved),
    (r"then", Tags.Reserved),
    (r"else", Tags.Reserved),
    (r"while", Tags.Reserved),
    (r"do", Tags.Reserved),
    (r"end", Tags.Reserved),
    (r"[0-9]+", Tags.Int),
    (r"[A-Za-z][A-Za-z0-9_]*", Tags.ID),
]


def imp_lex(characters):
    return lexer.lex(characters, token_exprs)
