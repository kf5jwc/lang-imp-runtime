import sys
import re
from typing import List, Tuple, Pattern
from enum import Enum
from . import exceptions
from .exceptions import NoMatchFound


# characters is an ~~iterable~~ indexable item which we can match against regex tokens, offered in token_exprs
def lex(characters, token_exprs) -> List[Tuple[str, Enum]]:
    position = 0
    ret_tokens = []
    exprs = precompile_tokens(token_exprs)
    chars_len = len(characters)

    while position < chars_len:
        ((token, tag), position) = find_next_expr(exprs, characters, position)
        if tag is not None:
            ret_tokens.append((token, tag))

    return ret_tokens


# Compile all expressions so we avoid doing it for each token
def precompile_tokens(given_tokens: List[Tuple[str, Enum]]) -> List[Tuple[Pattern[str], Enum]]:
    tokens: List[Tuple[Pattern[str], Enum]] = []
    for expr, tag in given_tokens:
        tokens.append((re.compile(expr), tag))
    return tokens


def find_next_expr(expressions, characters, position):
    for pattern, tag in expressions:
        match = pattern.match(characters, position)
        if match is not None:
            return ((match.group(0), tag), match.end(0))

    raise NoMatchFound(
        "Illegal character at position {}: {}".format(position, characters[position])
    )


__all__ = ["lex", "exceptions"]
