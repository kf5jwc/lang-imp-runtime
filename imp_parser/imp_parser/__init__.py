from ..parser import parser

def imp_parse(tokens):
    parser_func = parser()
    ast = parser_func(tokens, 0)
    return ast
